# libraries import

from sklearn.decomposition import NMF
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.preprocessing import normalize

from preprocess import *
from process import *
import time

import numpy as np
import json

# global NMF settings
rank = 5
tol = 1e-5
init = 'random'
solver = 'cd'  # for scikit
max_iter = 400

# dictionaries for: time, purity
times = {}
purities = {}
entropies = {}

# lodaing datasets
news1 = load_preprocessed_data('./results/Preprocessed_20news_150.txt')

# creating TF-IDF matrices for each dataset

vect1 = TfidfVectorizer()
Y1 = vect1.fit_transform(news1)
Y1norm = normalize(Y1)


"""
SCIKIT NMF CALCULATING
Parameters:
Factorization rank = 5
Max_iter = 200
Tol = 1e-5
Solver = coordinate distance
"""

scikit1 = NMF(n_components=rank, init=init, solver=solver, tol=tol, max_iter=max_iter)
print("Calculating NMF on BBC dataset 1 - 50 documents")
t0 = time.time()
A1 = scikit1.fit_transform(Y1norm)
X1 = scikit1.components_
elaps = time.time() - t0
# times.update({'{}1'.format(name): elaps})
print('Done in {}s'.format(elaps))

# purity calculating
# clusters_lists, clu_len = get_cluster_doc(A1)
# purity = get_purity_bbc(clusters_lists, rank, 0)
# print("Purity : {}".format(purity))
# purities.update({'{}1'.format(name): purity})
# entropy = get_entropy_bbc(clusters_lists, rank, 0)
# print("Entropy : {}".format(entropy))
# entropies.update({'{}1'.format(name): entropy})