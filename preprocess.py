import re
import unicodedata
import inflect
import os
from nltk.stem import LancasterStemmer, WordNetLemmatizer
from nltk import word_tokenize
from nltk.corpus import stopwords, reuters
from time import time


###########################
# DATASET HANDLING PART#
###########################
def load_bbc_datasets(dataset_no):
    # corpora list
    corps = []
    paths = ['./bbc-fulltext/bbc/business', './bbc-fulltext/bbc/entertainment', './bbc-fulltext/bbc/politics',
             './bbc-fulltext/bbc/sport', './bbc-fulltext/bbc/tech', './sandbox', './sandbox2', './sandbox3']

    t0 = time()
    # loading external text files as source for research
    for file in os.listdir(paths[dataset_no]):
        print("Loading file ", paths[dataset_no] + "/" + file)
        f = open(paths[dataset_no] + "/" + file, 'rU')
        text = f.read()
        corps.append(text)
        f.close()

    print("Dataset no {} loaded succesfully in {:03.3f}s!".format(dataset_no, time()-t0))
    return corps


def load_reuters_dataset():
    corps = []
    t0 = time()
    for fileid in reuters.fileids():
        print("Loading file {}".format(fileid))
        corps.append(reuters.raw(fileid))

    print("Dataset loaded successfully in {:03.3f}s!".format(time()-t0))
    return corps


def save_preprocessed_data(dataset, path):
    with open(path, 'w') as f:
        for el in dataset:
            f.write(el+"\n")
    return True


def load_preprocessed_data(path):
    dataset = []
    with open(path, 'r') as f:
        for l in f:
            dataset.append(l)
    return dataset


###########################
# TEXT PREPROCESSING PART #
###########################
def remove_non_ascii(words):
    """Remove non-ASCII characters from list of tokenized words"""
    new_words = []
    for word in words:
        new_word = unicodedata.normalize('NFKD', word).encode('ascii', 'ignore').decode('utf-8', 'ignore')
        new_words.append(new_word)
    return new_words


def to_lowercase(words):
    """Convert all characters to lowercase from list of tokenized words"""
    new_words = []
    for word in words:
        new_word = word.lower()
        new_words.append(new_word)
    return new_words


def remove_punctuation(words):
    """Remove punctuation from list of tokenized words"""
    new_words = []
    for word in words:
        new_word = re.sub(r'[^\w\s]', '', word)
        if new_word != '':
            new_words.append(new_word)
    return new_words


def replace_numbers(words):
    """Replace all interger occurrences in list of tokenized words with textual representation"""
    p = inflect.engine()
    new_words = []
    for word in words:
        if word.isdigit():
            new_word = p.number_to_words(word)
            new_words.append(new_word)
        else:
            new_words.append(word)
    return new_words


def remove_numbered_strings(words):
    """Removes all string which may contain numbers such as '100m', '87bn' etc. """
    new_words = []
    for word in words:
        if not any(char.isdigit() for char in word):
            new_words.append(word)
    return new_words


def remove_stopwords(words):
    """Remove stop words from list of tokenized words"""
    new_words = []
    for word in words:
        if word not in stopwords.words('english'):
            new_words.append(word)
    return new_words


def stem_words(words):
    """Stem words in list of tokenized words"""
    stemmer = LancasterStemmer()
    stems = []
    for word in words:
        stem = stemmer.stem(word)
        stems.append(stem)
    return stems


def lemmatize_verbs(words):
    """Lemmatize verbs in list of tokenized words"""
    lemmatizer = WordNetLemmatizer()
    lemmas = []
    for word in words:
        lemma = lemmatizer.lemmatize(word, pos='v')
        lemmas.append(lemma)
    return lemmas


def text_preparing(dataset, is20=False):
    print("Please wait, while text is being preprocessed")
    corps = []
    t0 = time()
    for text in dataset:
        temp = word_tokenize(text)
        if is20:
            temp = preprocess20(temp)
        else:
            temp = preprocess(temp)
        temp = ' '.join(temp)
        corps.append(temp)
    print("Done! Time: {:03.3f}s".format(time()-t0))
    return corps


def preprocess(words):
    words = remove_non_ascii(words)
    words = to_lowercase(words)
    words = remove_punctuation(words)
    words = replace_numbers(words)
    words = remove_numbered_strings(words)
    words = remove_stopwords(words)
    return words

def preprocess20(words):
    words = remove_non_ascii(words)
    words = to_lowercase(words)
    words = remove_punctuation(words)
    words = remove_numbered_strings(words)
    words = remove_stopwords(words)
    return words