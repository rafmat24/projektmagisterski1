from sklearn.datasets import fetch_20newsgroups

tests = fetch_20newsgroups(subset='test')

targets = tests.target[0:828]

with open('./source/20news_828.classes', 'w') as f:
    c = 0
    for r in targets:
        f.write("{} {}\n".format(c, r))
        c += 1