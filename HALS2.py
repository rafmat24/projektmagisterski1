import numpy as np
from numpy.linalg import norm
from preprocess import *
from process import *

eps = 1e-16
tol_proj = 1e-6
k_inner = 10


def hals_nmf(Y, rank, tol_obj, MaxIter):
    # factors and residuals initialization
    A = np.random.rand(Y.shape[0], rank)
    X = np.random.rand(rank, Y.shape[1])

    res = []
    res.append((norm(Y - A.dot(X), 'fro')) / (norm(Y, 'fro')))
    R = A.shape[1]


    # gradient calculating
    gradA = A.dot(X.dot(X.T)) - Y.dot(X.T)
    gradX = (A.T.dot(A)).dot(X) - A.T.dot(Y)

    # init grad stack
    init_grad = norm(np.vstack((gradA, gradX.T)), 'fro')

    tolA = max(0.00001, tol_proj)*init_grad
    tolX = tolA

    k_inner = 10

    for k in range(MaxIter):
        projnorm = norm(gradX[np.logical_or(gradX < 0, X > 0)])

        if projnorm < tol_proj*init_grad:
            break

        # X update
        X, gradX, iterX = hals_inner(A, Y, X, R, tolX, k_inner)
        if iterX == 1:
            tolX = 0.1*tolX

        # normalization
        # dX = eps+(np.sum(X, 1))
        # X = np.divide(dX.reshape(rank, 1), X)
        # A = np.multiply(dX.reshape(rank, 1).T, A)

        # Update for A
        At, gradA, iterA = hals_inner(X.T, Y.T, A.T, R, tolA, k_inner)
        A = At.T
        gradA = gradA.T
        if iterA == 1:
            tolA = 0.1*tolA

        # Normalization
        # dA = eps+(np.sum(A, 0))
        # A = np.divide(A.T, dA.reshape(Y.shape[0], 1))
        # X = np.multiply(dA.reshape(Y.shape[0], 1).T, X)

        res.append((norm(Y - A.dot(X), 'fro')) / (norm(Y, 'fro')))

        if k > 20:
            if ((res[k] - res[k + 1]) / res[0]) < tol_obj:
                print("Iteration: {}, Residual: {}, Diff_res: {}".format(k + 1, res[k + 1], res[k] - res[k + 1]))
                break

    return A, X, res


def hals_inner(A, Y, X, r, tol, k_inner):
    W = A.T.dot(Y)
    V = A.T.dot(A)

    for iter in range(k_inner):
        G = V.dot(W) - W
        projgrad = norm(G[np.logical_or(G < 0, X > 0)])
        if projgrad < tol:
            break

        for j in range(r):
            # X[j][:] = max(eps, np.divide(X[j][:]+(W[j][:] - V[j][:].dot(X)), (V[j][j])))
            X[j, :] = max(eps, X[j, :]+(W[j, :] - (V[j, :]).dot(X))/V[j, j])

    return X, G, iter