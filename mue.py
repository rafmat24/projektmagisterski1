import numpy.linalg as nla
import numpy as np
from numpy.linalg import norm


tol_proj = 1e-6
tol_obj = 1e-5
inner = 10
eps = 1e-16

def nmf_mue(Y, k, maxiter, tol):

    # init of matrices A and X
    A = np.random.rand(Y.shape[0], k)
    X = np.random.rand(k, Y.shape[1])

    # init of residual vector
    res = []
    res.append((nla.norm(Y - A.dot(X), 'fro')) / (norm(Y, 'fro')))

    # gradient calculating
    # gradient calculating
    gradA = A.dot(X.dot(X.T)) - Y.dot(X.T)
    gradX = (A.T.dot(A)).dot(X) - A.T.dot(Y)

    # init gradient vector
    init_grad = norm(np.vstack((gradA, gradX.T)), 'fro')

    # matrices toleration values
    tolA = max(0.00001, tol_proj) * init_grad
    tolX = tolA

    # main loop
    for i in range(maxiter):
        projnorm = norm(gradX[np.logical_or(gradX < 0, X > 0)])

        if projnorm < tol_proj * init_grad:
            break

        # update for X
        X, gradX, iterX = mue_inner(A, Y, X, tolX, inner)
        if iterX == 1:
            tolX = 0.1*tolX

        A, gradA, iterA = mue_inner(X.T ,Y.T, A.T, tolA,inner)
        A = A.T
        gradA = gradA.T
        if iterA == 1:
            tolA = 0.1 * tolA

        res.append((nla.norm(Y - A.dot(X), 'fro'))/(norm(Y, 'fro')))

        if k > 20:
            if((res[i] - res[i+1])/res[0]) < tol_obj:
                print("Iteration: {}, Residual: {}, Diff_res: {}".format(i+1, res[i+1], res[i]-res[i+1]))
                break

    return A, X, res


def mue_inner(A, Y, X, tol, inner):
    B = A.T.dot(A)
    Yt = A.T.dot(Y)

    for iter in range(inner):
        Z = B.dot(X)
        G = np.subtract(Z, Yt)

        projgrad = norm(G[np.logical_or(G < 0, X > 0)])
        if projgrad < tol:
            break

        X = np.multiply(X, np.divide(Yt, np.add(Z, eps)))

    return X, G, iter