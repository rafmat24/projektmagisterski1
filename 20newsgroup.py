from sklearn.datasets import *
from preprocess import *
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.preprocessing import normalize
from sklearn.decomposition import NMF
import time

# loading 20newsgroup dataset data - test dataset
raw = fetch_20newsgroups(subset='test')
print("Preprocessing 20newsgroup dataset test...")
t0 = time.time()
preprocessed = text_preparing(raw.data, True)
print("Done in {}s".format(time.time()-t0))
save_preprocessed_data(preprocessed, './results/Preprocessed_20news_test.txt')
print("Saved to ./results/Preprocessed_20news_test.txt")

# creating a TF-IDF matrix
vect = TfidfVectorizer()
Y = vect.fit_transform(preprocessed)
Y_norm = normalize(Y)


# calculating NMF
nmf = NMF(n_components=20, init='random', solver='cd', tol=1e-5, max_iter=100)
A = nmf.fit_transform(Y_norm)
X = nmf.components_




