# libraries import

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.preprocessing import normalize

from preprocess import *
from process import *
import time

import nimfa as nf

import numpy as np
import json


def runnimfanews(savematrices=True, savetimes=True, saveparams=True, name='nimfa'):
    # global NMF settings
    rank = 20
    tol = 1e-5
    init = 'random'
    solver = 'cd'  # for scikit
    max_iter = 400

    # dictionaries for: time, purity
    times = {}
    purities= {}
    residuals = {}
    entropies = {}

    # lodaing datasets
    news1 = load_preprocessed_data('./results/Preprocessed_20news_150.txt')
    news2 = load_preprocessed_data('./results/Preprocessed_20news_828.txt')
    news3 = load_preprocessed_data('./results/Preprocessed_20news_full.txt')

    # creating TF-IDF matrices for each dataset

    vect1 = TfidfVectorizer()
    Y1 = vect1.fit_transform(news1)
    Y1norm = normalize(Y1)

    vect2 = TfidfVectorizer()
    Y2 = vect2.fit_transform(news2)
    Y2norm = normalize(Y2)

    vect3 = TfidfVectorizer()
    Y3 = vect3.fit_transform(news3)
    Y3norm = normalize(Y3)

    """
        NIMFA NMF CALCULATING
        Parameters:
        Algorithm: ALS
        Factorization rank = 5
        Max_iter = 200
        Tol = 1e-5
        Solver = coordinate distance
    """

    nimfa1 = nf.Lsnmf(Y1norm, seed=init, rank=rank, max_iter=max_iter, min_residuals=tol, track_error=True)
    print("Calculating NMF on 20newsgroup dataset 1 - 150 documents")
    t0 = time.time()
    nimfa1fit = nimfa1()
    elaps = time.time() - t0
    times.update({'{}1'.format(name): elaps})
    print('Done in {}s'.format(elaps))
    residuals.update({'{}1_res'.format(name): nimfa1fit.fit.tracker._residuals[0]})

    A1 = nimfa1fit.fit.W.toarray()
    X1 = nimfa1fit.fit.H.toarray()

    # purity calculating
    clusters_lists, clu_len = get_cluster_doc(A1)
    purity = get_purity_20news(clusters_lists, rank, 0)
    print("Purity : {}".format(purity))
    purities.update({'{}1'.format(name): purity})
    entropy = get_entropy_20news(clusters_lists, rank, 0)
    print("Entropy : {}".format(entropy))
    entropies.update({'{}1'.format(name): entropy})

    nimfa2 = nf.Lsnmf(Y2norm, seed=init,rank=rank, max_iter=max_iter, min_residuals=tol, track_error=True)
    print("Calculating NMF on 20newsgroup dataset 2 - 828 documents")
    t0 = time.time()
    nimfa2fit = nimfa2()
    elaps = time.time() - t0
    times.update({'{}2'.format(name): elaps})
    print('Done in {}s'.format(elaps))
    residuals.update({'{}2_res'.format(name): nimfa2fit.fit.tracker._residuals[0]})

    A2 = nimfa2fit.fit.W.toarray()
    X2 = nimfa2fit.fit.H.toarray()

    # purity calculating
    clusters_lists, clu_len = get_cluster_doc(A2)
    purity = get_purity_20news(clusters_lists, rank, 1)
    print("Purity : {}".format(purity))
    purities.update({'{}2'.format(name): purity})
    entropy = get_entropy_20news(clusters_lists, rank, 1)
    print("Entropy : {}".format(entropy))
    entropies.update({'{}2'.format(name): entropy})

    nimfa3 = nf.Lsnmf(Y3norm, seed=init,rank=rank, max_iter=max_iter, min_residuals=tol, track_error=True)
    print("Calculating NMF on full 20newsgroup dataset - over 7000 documents")
    t0 = time.time()
    nimfa3fit = nimfa3()
    elaps = time.time() - t0
    times.update({'{}3'.format(name): elaps})
    print('Done in {}s'.format(elaps))
    residuals.update({'{}3_res'.format(name): nimfa3fit.fit.tracker._residuals[0]})

    A3 = nimfa3fit.fit.W.toarray()
    X3 = nimfa3fit.fit.H.toarray()

    # purity calculating
    clusters_lists, clu_len = get_cluster_doc(A3)
    purity = get_purity_20news(clusters_lists, rank, 2)
    print("Purity : {}".format(purity))
    purities.update({'{}3'.format(name): purity})
    entropy = get_entropy_20news(clusters_lists, rank, 2)
    print("Entropy : {}".format(entropy))
    entropies.update({'{}3'.format(name): entropy})


    if savematrices:
        matrices = {
            'A1{}'.format(name): A1,
            'X1{}'.format(name): X1,
            'A2{}'.format(name): A2,
            'X2{}'.format(name): X2,
            'A3{}'.format(name): A3,
            'X3{}'.format(name): X3,
        }

        np.savez('./matrices/news/news_{}.npz'.format(name), **matrices)

    if savetimes:
        # save times to json file
        with open('./results/times/news/times_{}.json'.format(name), 'w') as f:
            json.dump(times, f, sort_keys=True, indent=4)

    if saveparams:
        with open('./results/params/news/purities_{}.json'.format(name), 'w') as f:
            json.dump(purities, f, sort_keys=True, indent=4)

        with open('./results/params/news/res_{}.json'.format(name), 'w') as f:
            json.dump(residuals, f, sort_keys=True, indent=4)

        with open('./results/params/news/entropies_{}.json'.format(name), 'w') as f:
            json.dump(entropies, f, sort_keys=True, indent=4)
