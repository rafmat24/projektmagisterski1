# libraries import

from sklearn.decomposition import NMF
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.preprocessing import normalize

from preprocess import *
from process import *
import time

import numpy as np
import json


def runscikitbbc(savematrices=True, savetimes=True, saveparams=True, name='scikit'):
    # global NMF settings
    rank = 5
    tol = 1e-5
    init = 'random'
    solver = 'cd'  # for scikit
    max_iter = 400

    # dictionaries for: time, purity
    times = {}
    purities = {}
    entropies = {}

    # lodaing datasets
    bbc1 = load_preprocessed_data('./results/Preprocessed_bbc_50.txt')
    bbc2 = load_preprocessed_data('./results/Preprocessed_bbc_250.txt')
    bbc3 = load_preprocessed_data('./results/Preprocessed_bbc_full.txt')

    # creating TF-IDF matrices for each dataset

    vect1 = TfidfVectorizer()
    Y1 = vect1.fit_transform(bbc1)
    Y1norm = normalize(Y1)

    vect2 = TfidfVectorizer()
    Y2 = vect2.fit_transform(bbc2)
    Y2norm = normalize(Y2)

    vect3 = TfidfVectorizer()
    Y3 = vect3.fit_transform(bbc3)
    Y3norm = normalize(Y3)

    """
    SCIKIT NMF CALCULATING
    Parameters:
    Factorization rank = 5
    Max_iter = 200
    Tol = 1e-5
    Solver = coordinate distance
    """

    scikit1 = NMF(n_components=rank, init=init, solver=solver, tol=tol, max_iter=max_iter)
    print("Calculating NMF on BBC dataset 1 - 50 documents")
    t0 = time.time()
    A1 = scikit1.fit_transform(Y1norm)
    X1 = scikit1.components_
    elaps = time.time() - t0
    times.update({'{}1'.format(name): elaps})
    print('Done in {}s'.format(elaps))

    # purity calculating
    clusters_lists, clu_len = get_cluster_doc(A1)
    purity = get_purity_bbc(clusters_lists, rank, 0)
    print("Purity : {}".format(purity))
    purities.update({'{}1'.format(name): purity})
    entropy = get_entropy_bbc(clusters_lists, rank, 0)
    print("Entropy : {}".format(entropy))
    entropies.update({'{}1'.format(name): entropy})

    scikit2 = NMF(n_components=rank, init=init, solver=solver, tol=tol, max_iter=max_iter)
    print("Calculating NMF on BBC dataset 2 - 250 documents")
    t0 = time.time()
    A2 = scikit1.fit_transform(Y2norm)
    X2 = scikit1.components_
    elaps = time.time() - t0
    times.update({'{}2'.format(name): elaps})
    print('Done in {}s'.format(elaps))

    # purity calculating
    clusters_lists, clu_len = get_cluster_doc(A2)
    purity = get_purity_bbc(clusters_lists, rank, 1)
    print("Purity : {}".format(purity))
    purities.update({'{}2'.format(name): purity})
    entropy = get_entropy_bbc(clusters_lists, rank, 1)
    print("Entropy : {}".format(entropy))
    entropies.update({'{}2'.format(name): entropy})

    scikit3 = NMF(n_components=rank, init=init, solver=solver, tol=tol, max_iter=max_iter)
    print("Calculating NMF on BBC dataset 3 - full set of documents (2226)")
    t0 = time.time()
    A3 = scikit1.fit_transform(Y3norm)
    X3 = scikit1.components_
    elaps = time.time() - t0
    times.update({'{}3'.format(name): elaps})
    print('Done in {}s'.format(elaps))

    # purity calculating
    clusters_lists, clu_len = get_cluster_doc(A3)
    purity = get_purity_bbc(clusters_lists, rank, 2)
    print("Purity : {}".format(purity))
    purities.update({'{}3'.format(name): purity})
    entropy = get_entropy_bbc(clusters_lists, rank, 2)
    print("Entropy : {}".format(entropy))
    entropies.update({'{}3'.format(name): entropy})

    if savematrices:
        matrices = {
            'A1{}'.format(name): A1,
            'X1{}'.format(name): X1,
            'A2{}'.format(name): A2,
            'X2{}'.format(name): X2,
            'A3{}'.format(name): A3,
            'X3{}'.format(name): X3,
        }

        np.savez('./matrices/BBC/BBC_{}.npz'.format(name), **matrices)

    if savetimes:
        # save times to json file
        with open('./results/times/times_{}.json'.format(name), 'w') as f:
            json.dump(times, f, sort_keys=True, indent=4)

    if saveparams:
        with open('./results/params/purities_{}.json'.format(name), 'w') as f:
            json.dump(purities, f,  sort_keys=True, indent=4)

        with open('./results/params/entropies_{}.json'.format(name), 'w') as f:
            json.dump(entropies, f, sort_keys=True, indent=4)


