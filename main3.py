from news_scikit1 import runscikitnews
from news_nimfa1 import runnimfanews
from news_mue1 import runmuenews
from news_hals1 import runhalsnews


print("RUN SCKITI ALGORITHMS")
runscikitnews(True, True, True, 'scikit1')

print("RUN NIMFA ALGORITHMS")
runnimfanews(True, True, True, 'nimfa1')

# print("RUN MUE ALGORITHMS")
# runmuenews(True, True, True, 'mue1')

# print("RUN HALS ALGORITHMS")
# runhalsnews(True, True, True, 'hals1')
