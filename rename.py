import os

counter = 1826
path = './bbc-fulltext/bbc/tech/'
for file in os.listdir(path):
    newname = '{counter}.txt'.format(counter=counter)
    print(counter, newname)
    os.rename(os.path.join(path, file), os.path.join(path, newname))
    counter += 1
