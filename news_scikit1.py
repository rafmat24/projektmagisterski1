# libraries import

from sklearn.decomposition import NMF
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.preprocessing import normalize

from preprocess import *
from process import *
import time

import numpy as np
import json


def runscikitnews(savematrices=True, savetimes=True, saveparams=True, name='scikit'):
    # global NMF settings
    rank = 20
    tol = 1e-5
    init = 'random'
    solver = 'cd'  # for scikit
    max_iter = 400

    # dictionaries for: time, purity
    times = {}
    purities = {}
    entropies = {}

    # lodaing datasets
    news1 = load_preprocessed_data('./results/Preprocessed_20news_150.txt')
    news2 = load_preprocessed_data('./results/Preprocessed_20news_828.txt')
    news3 = load_preprocessed_data('./results/Preprocessed_20news_full.txt')

    # creating TF-IDF matrices for each dataset
    vect1 = TfidfVectorizer()
    Y1 = vect1.fit_transform(news1)
    Y1norm = normalize(Y1)

    vect2 = TfidfVectorizer()
    Y2 = vect2.fit_transform(news2)
    Y2norm = normalize(Y2)

    vect3 = TfidfVectorizer()
    Y3 = vect3.fit_transform(news3)
    Y3norm = normalize(Y3)

    """
    SCIKIT NMF CALCULATING
    Parameters:
    Factorization rank = 20
    Max_iter = 200
    Tol = 1e-5
    Solver = coordinate distance
    """

    scikit1 = NMF(n_components=rank, init=init, solver=solver, tol=tol, max_iter=max_iter)
    print("Calculating NMF on 20newsgroup dataset 1 - 150 documents")
    t0 = time.time()
    A1 = scikit1.fit_transform(Y1norm)
    X1 = scikit1.components_
    elaps = time.time() - t0
    times.update({'{}1'.format(name): elaps})
    print('Done in {}s'.format(elaps))

    # purity calculating
    clusters_lists, clu_len = get_cluster_doc(A1)
    purity = get_purity_20news(clusters_lists, rank, 0)
    print("Purity : {}".format(purity))
    purities.update({'{}1'.format(name): purity})
    entropy = get_entropy_20news(clusters_lists, rank, 0)
    print("Entropy : {}".format(entropy))
    entropies.update({'{}1'.format(name): entropy})

    scikit2 = NMF(n_components=rank, init=init, solver=solver, tol=tol, max_iter=max_iter)
    print("Calculating NMF on 20newsgroup dataset 2 - 828 documents")
    t0 = time.time()
    A2 = scikit1.fit_transform(Y2norm)
    X2 = scikit1.components_
    elaps = time.time() - t0
    times.update({'{}2'.format(name): elaps})
    print('Done in {}s'.format(elaps))

    # purity calculating
    clusters_lists, clu_len = get_cluster_doc(A2)
    purity = get_purity_20news(clusters_lists, rank, 1)
    print("Purity : {}".format(purity))
    purities.update({'{}2'.format(name): purity})
    entropy = get_entropy_20news(clusters_lists, rank, 1)
    print("Entropy : {}".format(entropy))
    entropies.update({'{}2'.format(name): entropy})

    scikit3 = NMF(n_components=rank, init=init, solver=solver, tol=tol, max_iter=max_iter)
    print("Calculating NMF on 20newsgroup dataset 3 - full set of documents (over 7000)")
    t0 = time.time()
    A3 = scikit1.fit_transform(Y3norm)
    X3 = scikit1.components_
    elaps = time.time() - t0
    times.update({'{}3'.format(name): elaps})
    print('Done in {}s'.format(elaps))

    # purity calculating
    clusters_lists, clu_len = get_cluster_doc(A3)
    purity = get_purity_20news(clusters_lists, rank, 2)
    print("Purity : {}".format(purity))
    purities.update({'{}3'.format(name): purity})
    entropy = get_entropy_20news(clusters_lists, rank, 2)
    print("Entropy : {}".format(entropy))
    entropies.update({'{}3'.format(name): entropy})

    if savematrices:
        matrices = {
            'A1{}'.format(name): A1,
            'X1{}'.format(name): X1,
            'A2{}'.format(name): A2,
            'X2{}'.format(name): X2,
            'A3{}'.format(name): A3,
            'X3{}'.format(name): X3,
        }

        np.savez('./matrices/news/news_{}.npz'.format(name), **matrices)

    if savetimes:
        # save times to json file
        with open('./results/times/news/times_{}.json'.format(name), 'w') as f:
            json.dump(times, f, sort_keys=True, indent=4)

    if saveparams:
        with open('./results/params/news/purities_{}.json'.format(name), 'w') as f:
            json.dump(purities, f,  sort_keys=True, indent=4)

        with open('./results/params/news/entropies_{}.json'.format(name), 'w') as f:
            json.dump(entropies, f, sort_keys=True, indent=4)
