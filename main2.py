from bbc_scikit1 import runscikitbbc
from bbc_nimfa1 import runnimfabbc
from bbc_mue1 import runmuebbc
from bbc_hals1 import runhalsbbc


print("RUN SCKITI ALGORITHMS")
runscikitbbc(True, True, True, 'scikit5')

print("RUN NIMFA ALGORITHMS")
runnimfabbc(True, True, True, 'nimfa5')

print("RUN MUE ALGORITHMS")
runmuebbc(True, True, True, 'mue5')

print("RUN HALS ALGORITHMS")
runhalsbbc(True, True, True, 'hals5')
