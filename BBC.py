# libraries import

from sklearn.decomposition import NMF
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.preprocessing import normalize

from mue import nmf_mue
from HALS import NMF_HALS

from preprocess import *
from process import *
import time

import nimfa as nf

import numpy as np
import json


def runbbc(savematrices=True, savetimes=True):
    # global NMF settings
    rank = 5
    tol = 1e-5
    init = 'random'
    solver = 'cd'  # for scikit
    max_iter = 400

    # dictionaries for: time, purity
    times = {}
    purity = {}

    # lodaing datasets
    bbc1 = load_preprocessed_data('./results/Preprocessed_bbc_50.txt')
    bbc2 = load_preprocessed_data('./results/Preprocessed_bbc_250.txt')
    bbc3 = load_preprocessed_data('./results/Preprocessed_bbc_full.txt')

    # creating TF-IDF matrices for each dataset

    vect1 = TfidfVectorizer()
    Y1 = vect1.fit_transform(bbc1)
    Y1norm = normalize(Y1)

    vect2 = TfidfVectorizer()
    Y2 = vect2.fit_transform(bbc2)
    Y2norm = normalize(Y2)

    vect3 = TfidfVectorizer()
    Y3 = vect3.fit_transform(bbc3)
    Y3norm = normalize(Y3)

    """
    SCIKIT NMF CALCULATING
    Parameters:
    Factorization rank = 5
    Max_iter = 200
    Tol = 1e-5
    Solver = coordinate distance
    """

    # scikit1 = NMF(n_components=rank, init=init, solver=solver, tol=tol, max_iter=max_iter)
    # print("Calculating NMF on BBC dataset 1 - 50 documents")
    # t0 = time.time()
    # A1 = scikit1.fit_transform(Y1norm)
    # X1 = scikit1.components_
    # elaps = time.time() - t0
    # times.update({'scikit1': elaps})
    # print('Done in {}s'.format(elaps))
    #
    # # purity calculating
    # clusters_lists, clu_len = get_cluster_doc(A1)
    # purity = get_purity_bbc(clusters_lists, rank, 0)
    # print("Purity : {}".format(purity))
    #
    # scikit2 = NMF(n_components=rank, init=init, solver=solver, tol=tol, max_iter=max_iter)
    # print("Calculating NMF on BBC dataset 2 - 250 documents")
    # t0 = time.time()
    # A2 = scikit1.fit_transform(Y2norm)
    # X2 = scikit1.components_
    # elaps = time.time() - t0
    # times.update({'scikit2': elaps})
    # print('Done in {}s'.format(elaps))
    #
    # # purity calculating
    # clusters_lists, clu_len = get_cluster_doc(A2)
    # purity = get_purity_bbc(clusters_lists, rank, 1)
    # print("Purity : {}".format(purity))
    #
    # scikit3 = NMF(n_components=rank, init=init, solver=solver, tol=tol, max_iter=max_iter)
    # print("Calculating NMF on BBC dataset 3 - full set of documents (2226)")
    # t0 = time.time()
    # A3 = scikit1.fit_transform(Y3norm)
    # X3 = scikit1.components_
    # elaps = time.time() - t0
    # times.update({'scikit3': elaps})
    # print('Done in {}s'.format(elaps))
    #
    # # purity calculating
    # clusters_lists, clu_len = get_cluster_doc(A3)
    # purity = get_purity_bbc(clusters_lists, rank, 3)
    # print("Purity : {}".format(purity))
    #
    # """
    # NIMFA NMF CALCULATING
    # Parameters:
    # Algorithm: ALS
    # Factorization rank = 5
    # Max_iter = 200
    # Tol = 1e-5
    # Solver = coordinate distance
    # """
    #
    # nimfa1 = nf.Lsnmf(Y1norm, seed=init, rank=rank, max_iter=max_iter, min_residuals=tol, track_error=True)
    # print("Calculating NMF on BBC dataset 1 - 50 documents")
    # t0 = time.time()
    # nimfa1fit = nimfa1()
    # elaps = time.time() - t0
    # times.update({'nimfa1': elaps})
    # print('Done in {}s'.format(elaps))
    #
    # A4 = nimfa1fit.fit.W.toarray()
    # X4 = nimfa1fit.fit.H.toarray()
    #
    # # purity calculating
    # clusters_lists, clu_len = get_cluster_doc(A4)
    # purity = get_purity_bbc(clusters_lists, rank, 0)
    # print("Purity : {}".format(purity))
    #
    # nimfa2 = nf.Lsnmf(Y2norm, seed=init,rank=rank, max_iter=max_iter, min_residuals=tol, track_error=True)
    # print("Calculating NMF on BBC dataset 2 - 250 documents")
    # t0 = time.time()
    # nimfa2fit = nimfa2()
    # elaps = time.time() - t0
    # times.update({'nimfa2': elaps})
    # print('Done in {}s'.format(elaps))
    #
    # A5 = nimfa2fit.fit.W.toarray()
    # X5 = nimfa2fit.fit.H.toarray()
    #
    # # purity calculating
    # clusters_lists, clu_len = get_cluster_doc(A5)
    # purity = get_purity_bbc(clusters_lists, rank, 1)
    # print("Purity : {}".format(purity))
    #
    # nimfa3 = nf.Lsnmf(Y3norm, seed=init,rank=rank, max_iter=max_iter, min_residuals=tol, track_error=True)
    # print("Calculating NMF on full BBC dataset - 2226 documents")
    # t0 = time.time()
    # nimfa3fit = nimfa3()
    # elaps = time.time() - t0
    # times.update({'nimfa3': elaps})
    # print('Done in {}s'.format(elaps))
    #
    # A6 = nimfa3fit.fit.W.toarray()
    # X6 = nimfa3fit.fit.H.toarray()
    #
    # # purity calculating
    # clusters_lists, clu_len = get_cluster_doc(A6)
    # purity = get_purity_bbc(clusters_lists, rank, 2)
    # print("Purity : {}".format(purity))
    #
    # """
    # MUE NMF CALCULATING
    # Parameters:
    # Algorithm: ALS
    # Factorization rank = 5
    # Max_iter = 200
    # Tol = 1e-5
    # Solver = coordinate distance
    # """
    #
    # t0 = time.time()
    # print("Calculating NMF on BBC dataset 1- 50 documents")
    # A7, X7, res7 = nmf_mue(Y1norm.toarray(), rank, max_iter, tol)
    # elaps = time.time() - t0
    # times.update({'mue1': elaps})
    # print('Done in {}s'.format(elaps))
    #
    # # purity calculating
    # clusters_lists, clu_len = get_cluster_doc(A7)
    # purity = get_purity_bbc(clusters_lists, rank, 0)
    # print("Purity : {}".format(purity))
    #
    # t0 = time.time()
    # print("Calculating NMF on BBC dataset 2- 250 documents")
    # A8, X8, res8 = nmf_mue(Y2norm.toarray(), rank, max_iter, tol)
    # elaps = time.time() - t0
    # times.update({'mue2': elaps})
    # print('Done in {}s'.format(elaps))
    #
    # # purity calculating
    # clusters_lists, clu_len = get_cluster_doc(A8)
    # purity = get_purity_bbc(clusters_lists, rank, 1)
    # print("Purity : {}".format(purity))
    #
    #
    # t0 = time.time()
    # print("Calculating NMF on full BBC dataset - 2226 documents")
    # A9, X9, res9 = nmf_mue(Y3norm.toarray(), rank, max_iter, tol)
    # elaps = time.time() - t0
    # times.update({'mue3': elaps})
    # print('Done in {}s'.format(elaps))
    #
    # # purity calculating
    # clusters_lists, clu_len = get_cluster_doc(A9)
    # purity = get_purity_bbc(clusters_lists, rank, 2)
    # print("Purity : {}".format(purity))

    """
        HALS NMF CALCULATING
        Parameters:
        Algorithm: HALS
        Factorization rank = 5
        Max_iter = 200
        Tol = 1e-5
        Solver = coordinate distance
    """

    t0 = time.time()
    A10, X10, res10 = NMF_HALS().run(Y1norm.toarray(), rank, max_iter=max_iter)
    print('Done! Time: {}s'.format(time.time() - t0))

    # purity calculating
    clusters_lists, clu_len = get_cluster_doc(A10)
    purity = get_purity_bbc(clusters_lists, rank, 0)
    print("Purity : {}".format(purity))

    t0 = time.time()
    A11, X11, res11 = NMF_HALS().run(Y2norm.toarray(), rank, max_iter=max_iter)
    print('Done! Time: {}s'.format(time.time() - t0))

    # purity calculating
    clusters_lists, clu_len = get_cluster_doc(A11)
    purity = get_purity_bbc(clusters_lists, rank, 1)
    print("Purity : {}".format(purity))

    t0 = time.time()
    A12, X12, res12 = NMF_HALS().run(Y3norm.toarray(), rank, max_iter=max_iter)
    print('Done! Time: {}s'.format(time.time() - t0))

    # purity calculating
    clusters_lists, clu_len = get_cluster_doc(A12)
    purity = get_purity_bbc(clusters_lists, rank, 2)
    print("Purity : {}".format(purity))

    if savematrices:
        matrices = {
            # 'A1': A1,
            # 'X1': X1,
            # 'A2': A2,
            # 'X2': X2,
            # 'A3': A3,
            # 'X3': X3,
            # 'A4': A4,
            # 'X4': X4,
            # 'A5': A5,
            # 'X5': X5,
            # 'A6': A6,
            # 'X6': X6,
            # 'A7': A7,
            # 'X7': X7,
            # 'A8': A8,
            # 'X8': X8,
            # 'A9': A9,
            # 'X9': X9,
            'A10': A10,
            'X10': X10,
            'A11': A11,
            'X11': X11,
            'A12': A12,
            'X12': X12
        }

        # save resultant matrices to npz file
        np.savez('./matrices/BBC/BBC_HALS.npz', **matrices)
        # np.savez('./matrices/BBC.npz', A1=A1, X1=X1, A2=A2, X2=X2, A3=A3, X3=X3, A4=A4, X5=X5,
        #         A6=A6, X6=X6, A7=A7, X7=X7, A8=A8, X8=X8, A9=A9, X9=X9, Y1=Y1norm.toarray(), Y2=Y2norm.toarray(),
        #          Y3=Y3norm.toarray())

    if savetimes:
        # save times to json file
        with open('./results/times_HALS.json', 'w') as f:
            json.dump(times, f, sort_keys=True, indent=4)


runbbc(True, True)
