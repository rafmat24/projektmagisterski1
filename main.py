from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.cluster import KMeans
from sklearn.decomposition import NMF
from sklearn.preprocessing import normalize
from preprocess import *
from process import *


# loading data
raw_text_data = load_bbc_datasets(5)
text_data = text_preparing(raw_text_data)

# saving preprocessed data for future use - TBU
save_preprocessed_data(text_data, './results/Preprocessed_bbc.txt')

#############################
# TF-IDF Vectorizer for NMF #
#############################
tf_vectorizer = TfidfVectorizer()
Y = tf_vectorizer.fit_transform(text_data)
Y_norm = normalize(Y)

##############################
# CountVectorizer for KMeans #
##############################
cv = CountVectorizer(stop_words='english', max_features=500)
transformed = cv.fit_transform(text_data)
transformed_norm = normalize(transformed)

##########################################
# NMF set no. 1 - n_components = 5       #
# init=random, solver='mu', l1_ratio=0.5 #
##########################################
print("NMF with parameters n_components = 5, init=random, solver='mu', l1_ratio=0.5")
nmf = NMF(n_components=5, init='random', solver='mu', l1_ratio=0.5)
A = nmf.fit_transform(Y_norm)
X = nmf.components_
features = tf_vectorizer.get_feature_names()
clusters, clu_num = get_clusters(A)
cluster_lists = get_cluster_doc(clusters, clu_num, './results/Clustering_NMF_1.txt', True)

save2csv(Y_norm, A, X, clu_num, features, ['Y1', 'A1', 'X1'])

##########################################
# NMF set no. 2 - n_components = 5       #
# init=nndsvd, solver='cd', l1_ratio=0.5 #
##########################################
print("NMF with parameters n_components = 5, init=nndsvd, solver='cd', l1_ratio=0.5")
nmf = NMF(n_components=5, init='nndsvd', solver='cd', l1_ratio=0.5)
A = nmf.fit_transform(Y_norm)
X = nmf.components_
features = tf_vectorizer.get_feature_names()
clusters, clu_num = get_clusters(A)
cluster_lists = get_cluster_doc(clusters, clu_num, './results/Clustering_NMF_2.txt', True)

save2csv(Y_norm, A, X, clu_num, features, ['Y2', 'A2', 'X2'])

#########################################
# NMF set no. 3 - n_components = 5       #
# init=random, solver='cd', l1_ratio=0.5
# max_iter = 100, tol = 1e-6 #
##########################################
print("NMF with parameters n_components = 5, init=random, solver='cd', l1_ratio=0.5, max_iter=100, tol=1e-6")
nmf = NMF(n_components=5, init='random', solver='cd', l1_ratio=0.5, max_iter=100, tol=1e-6)
A = nmf.fit_transform(Y_norm)
X = nmf.components_
features = tf_vectorizer.get_feature_names()
clusters, clu_num = get_clusters(A)
cluster_lists = get_cluster_doc(clusters, clu_num, './results/Clustering_NMF_3.txt', True)

save2csv(Y_norm, A, X, clu_num, features, ['Y3', 'A3', 'X3'])

#############################################
# Kmeans set no. 1 - n_components = 5       #
#############################################
print("KMeans with parameters n_clusters = 5, rest parameters - default")
km = KMeans(n_clusters=5)
fitted = km.fit_transform(transformed_norm)
clusters, clu_num = get_clusters(fitted)
cluster_lists = get_cluster_doc(clusters, clu_num, './results/Clustering_KMeans.txt', True)
