# libraries import

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.preprocessing import normalize

from preprocess import *
from process import *
import time

from mue import nmf_mue

import numpy as np
import json


def runmuebbc(savematrices=True, savetimes=True, saveparams=True, name='mue'):
    # global NMF settings
    rank = 5
    tol = 1e-5
    init = 'random'
    solver = 'cd'  # for scikit
    max_iter = 400

    # dictionaries for: time, purity
    times = {}
    purities = {}
    residuals = {}
    entropies = {}

    # lodaing datasets
    bbc1 = load_preprocessed_data('./results/Preprocessed_bbc_50.txt')
    bbc2 = load_preprocessed_data('./results/Preprocessed_bbc_250.txt')
    bbc3 = load_preprocessed_data('./results/Preprocessed_bbc_full.txt')

    # creating TF-IDF matrices for each dataset

    vect1 = TfidfVectorizer()
    Y1 = vect1.fit_transform(bbc1)
    Y1norm = normalize(Y1)

    vect2 = TfidfVectorizer()
    Y2 = vect2.fit_transform(bbc2)
    Y2norm = normalize(Y2)

    vect3 = TfidfVectorizer()
    Y3 = vect3.fit_transform(bbc3)
    Y3norm = normalize(Y3)

    """
    MUE NMF CALCULATING
    Parameters:
    Algorithm: MUE
    Factorization rank = 5
    Max_iter = 200
    Tol = 1e-5
    Solver = coordinate distance
    """

    t0 = time.time()
    print("Calculating NMF on BBC dataset 1- 50 documents")
    A1, X1, res1 = nmf_mue(Y1norm.toarray(), rank, max_iter, tol)
    elaps = time.time() - t0
    times.update({'{}1'.format(name): elaps})
    print('Done in {}s'.format(elaps))
    residuals.update({'{}1_res'.format(name): res1})

    # purity calculating
    clusters_lists, clu_len = get_cluster_doc(A1)
    purity = get_purity_bbc(clusters_lists, rank, 0)
    print("Purity : {}".format(purity))
    purities.update({'{}1'.format(name): purity})
    entropy = get_entropy_bbc(clusters_lists, rank, 0)
    print("Entropy : {}".format(entropy))
    entropies.update({'{}1'.format(name): entropy})


    t0 = time.time()
    print("Calculating NMF on BBC dataset 2- 250 documents")
    A2, X2, res2 = nmf_mue(Y2norm.toarray(), rank, max_iter, tol)
    elaps = time.time() - t0
    times.update({'{}2'.format(name): elaps})
    print('Done in {}s'.format(elaps))
    residuals.update({'{}2_res'.format(name): res2})

    # purity calculating
    clusters_lists, clu_len = get_cluster_doc(A2)
    purity = get_purity_bbc(clusters_lists, rank, 1)
    print("Purity : {}".format(purity))
    purities.update({'{}2'.format(name): purity})
    entropy = get_entropy_bbc(clusters_lists, rank, 1)
    print("Entropy : {}".format(entropy))
    entropies.update({'{}2'.format(name): entropy})

    t0 = time.time()
    print("Calculating NMF on full BBC dataset - 2226 documents")
    A3, X3, res3 = nmf_mue(Y3norm.toarray(), rank, max_iter, tol)
    elaps = time.time() - t0
    times.update({'{}3'.format(name): elaps})
    print('Done in {}s'.format(elaps))
    residuals.update({'{}3_res'.format(name): res3})

    # purity calculating
    clusters_lists, clu_len = get_cluster_doc(A3)
    purity = get_purity_bbc(clusters_lists, rank, 2)
    print("Purity : {}".format(purity))
    purities.update({'{}3'.format(name): purity})
    entropy = get_entropy_bbc(clusters_lists, rank, 2)
    print("Entropy : {}".format(entropy))
    entropies.update({'{}3'.format(name): entropy})

    if savematrices:
        matrices = {
            'A1{}'.format(name): A1,
            'X1{}'.format(name): X1,
            'A2{}'.format(name): A2,
            'X2{}'.format(name): X2,
            'A3{}'.format(name): A3,
            'X3{}'.format(name): X3,
        }

        np.savez('./matrices/BBC/BBC_{}.npz'.format(name), **matrices)

    if savetimes:
        # save times to json file
        with open('./results/times/times_{}.json'.format(name), 'w') as f:
            json.dump(times, f, sort_keys=True, indent=4)

    if saveparams:
        with open('./results/params/purities_{}.json'.format(name), 'w') as f:
            json.dump(purities, f, sort_keys=True, indent=4)
        with open('./results/params/res_{}.json'.format(name), 'w') as f:
            json.dump(residuals, f, sort_keys=True, indent=4)

        with open('./results/params/entropies_{}.json'.format(name), 'w') as f:
            json.dump(entropies, f, sort_keys=True, indent=4)
