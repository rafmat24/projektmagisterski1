import pandas as pd
import math


##########################################################
# Function for hard clustering documents from matrix     #
# Input:                                                 #
#   array - input cluster array A form NMF               #
# Output:                                                #
#   cluster_arr - array of lists in form [doc, cluster]  #
##########################################################
def get_clusters(array):
    cluster_arr = []
    for idx, row in enumerate(array):
        cluster = [idx, row.argmax()]
        cluster_arr.append(cluster)
    return cluster_arr


##########################################################
# Function for retrieving document membership to each    #
# cluster                                                #
# Input:                                                 #
#   array - input cluster array A form NMF               #
# Output:                                                #
#   cluster_lists - cluster list memebership dictionary  #
#       in form {Cx : [doc1, doc2,...docn]}              #
#   cluster_len   - length of each obtained cluster      #
##########################################################
def get_cluster_doc(array):
    clusters_len = []
    cluster_num = array.shape[1]
    cluster_arr = get_clusters(array)
    cluster_lists = {'C{}'.format(i): [] for i in range(cluster_num)}
    for k, v in cluster_lists.items():
        for el in cluster_arr:
            if el[1] == int(k[1]):
                v.append(el[0])
        clusters_len.append(len(v))
    return cluster_lists, clusters_len


####################################################
# Function for getting true labels for BBC dataset
# Input:
#   path - path to file .classes with true labels
#   clu_num - number of clusters
# Output:
#   cluster_list#
####################################################
def get_bbc_tlabels(path, clu_num):
    cluster_lists = {'C{}'.format(i): [] for i in range(clu_num)}
    with open(path, 'r') as f:
        for row in f:
            s = row.split(" ")
            s = [int(s[0]), int(s[1])]
            if s[1] == 0:
                cluster_lists['C0'].append(s[0])
            elif s[1] == 1:
                cluster_lists['C1'].append(s[0])
            elif s[1] == 2:
                cluster_lists['C2'].append(s[0])
            elif s[1] == 3:
                cluster_lists['C3'].append(s[0])
            elif s[1] == 4:
                cluster_lists['C4'].append(s[0])

    return cluster_lists


##########################################################
# Function for getting true labels for 20newsgroup dataset
# Input:
#   path - path to file .classes with true labels
#   clu_num - number of clusters
# Output:
#   cluster_list#
##########################################################
def get_20newsgroup_tlabels(path, clu_num):
    cluster_lists = {'C{}'.format(i): [] for i in range(clu_num)}
    with open(path, 'r') as f:
        for row in f:
            s = row.split(" ")
            s = [int(s[0]), int(s[1])]
            if s[1] == 0:
                cluster_lists['C0'].append(s[0])
            elif s[1] == 1:
                cluster_lists['C1'].append(s[0])
            elif s[1] == 2:
                cluster_lists['C2'].append(s[0])
            elif s[1] == 3:
                cluster_lists['C3'].append(s[0])
            elif s[1] == 4:
                cluster_lists['C4'].append(s[0])
            elif s[1] == 5:
                cluster_lists['C5'].append(s[0])
            elif s[1] == 6:
                cluster_lists['C6'].append(s[0])
            elif s[1] == 7:
                cluster_lists['C7'].append(s[0])
            elif s[1] == 8:
                cluster_lists['C8'].append(s[0])
            elif s[1] == 9:
                cluster_lists['C9'].append(s[0])
            elif s[1] == 10:
                cluster_lists['C10'].append(s[0])
            elif s[1] == 11:
                cluster_lists['C11'].append(s[0])
            elif s[1] == 12:
                cluster_lists['C12'].append(s[0])
            elif s[1] == 13:
                cluster_lists['C13'].append(s[0])
            elif s[1] == 14:
                cluster_lists['C14'].append(s[0])
            elif s[1] == 15:
                cluster_lists['C15'].append(s[0])
            elif s[1] == 16:
                cluster_lists['C16'].append(s[0])
            elif s[1] == 17:
                cluster_lists['C17'].append(s[0])
            elif s[1] == 18:
                cluster_lists['C18'].append(s[0])
            elif s[1] == 19:
                cluster_lists['C19'].append(s[0])

    return cluster_lists


#########################################################
# Function for getting purity clusters from BBC dataset #
#########################################################
def get_purity_bbc(cluster_lists, cluster_num, dataset_no):
    # retrieve original ccluster dictionary
    paths = ['./bbc/bbc_50.classes', './bbc/bbc_250.classes', './bbc/bbc_full.classes']

    tlabels = get_bbc_tlabels(paths[dataset_no], 5)

    # list for
    max_list = []
    for k, v in cluster_lists.items():
        temp = {'C{}'.format(i): [] for i in range(cluster_num)}
        for it in v:
            if it in tlabels['C0']:
                temp['C0'].append(it)
            elif it in tlabels['C1']:
                temp['C1'].append(it)
            elif it in tlabels['C2']:
                temp['C2'].append(it)
            elif it in tlabels['C3']:
                temp['C3'].append(it)
            elif it in tlabels['C4']:
                temp['C4'].append(it)
        lengths = []
        for key, val in temp.items():
            lengths.append(len(val))
        max_list.append(max(lengths))
    return sum(max_list)/sum(len(v) for v in cluster_lists.values())


#################################################################
# Function for getting purity clusters from 20newsgroup dataset #
#################################################################
def get_purity_20news(cluster_lists, cluster_num, dataset_no):
    # retrieve original ccluster dictionary
    paths = ['./source/20news_150.classes', './source/20news_828.classes', './source/20news_full.classes']

    tlabels = get_20newsgroup_tlabels(paths[dataset_no], 20)

    # list for
    max_list = []
    for k, v in cluster_lists.items():
        temp = {'C{}'.format(i): [] for i in range(cluster_num)}
        for it in v:
            if it in tlabels['C0']:
                temp['C0'].append(it)
            elif it in tlabels['C1']:
                temp['C1'].append(it)
            elif it in tlabels['C2']:
                temp['C2'].append(it)
            elif it in tlabels['C3']:
                temp['C3'].append(it)
            elif it in tlabels['C4']:
                temp['C4'].append(it)
            elif it in tlabels['C5']:
                temp['C5'].append(it)
            elif it in tlabels['C6']:
                temp['C6'].append(it)
            elif it in tlabels['C7']:
                temp['C7'].append(it)
            elif it in tlabels['C8']:
                temp['C8'].append(it)
            elif it in tlabels['C9']:
                temp['C9'].append(it)
            elif it in tlabels['C10']:
                temp['C10'].append(it)
            elif it in tlabels['C11']:
                temp['C11'].append(it)
            elif it in tlabels['C12']:
                temp['C12'].append(it)
            elif it in tlabels['C13']:
                temp['C13'].append(it)
            elif it in tlabels['C14']:
                temp['C14'].append(it)
            elif it in tlabels['C15']:
                temp['C15'].append(it)
            elif it in tlabels['C16']:
                temp['C16'].append(it)
            elif it in tlabels['C17']:
                temp['C17'].append(it)
            elif it in tlabels['C18']:
                temp['C18'].append(it)
            elif it in tlabels['C19']:
                temp['C19'].append(it)
        lengths = []
        for key, val in temp.items():
            lengths.append(len(val))
        max_list.append(max(lengths))
    return sum(max_list)/sum(len(v) for v in cluster_lists.values())


def get_entropy_bbc(cluster_lists, cluster_num, dataset_no):
    tot_ent = 0
    sub_ent = 0
    docs_len = 0

    # retrieve original ccluster dictionary
    paths = ['./bbc/bbc_50.classes', './bbc/bbc_250.classes', './bbc/bbc.classes', './source/20news_150.classes',
             './source/20news_828.classes', './source/20news_full.classes']
    for k, v in cluster_lists.items():
        docs_len += len(v)

    tlabels = get_bbc_tlabels(paths[dataset_no], 5)

    # list for
    max_list = []
    for k, v in cluster_lists.items():
        temp = {'C{}'.format(i): [] for i in range(cluster_num)}
        for it in v:
            if it in tlabels['C0']:
                temp['C0'].append(it)
            elif it in tlabels['C1']:
                temp['C1'].append(it)
            elif it in tlabels['C2']:
                temp['C2'].append(it)
            elif it in tlabels['C3']:
                temp['C3'].append(it)
            elif it in tlabels['C4']:
                temp['C4'].append(it)
        for k1, v1 in temp.items():
            if len(v1) == 0:
                pass
            else:
                sub_ent += (len(v1)/len(v))*math.log(len(v1)/len(v), 2)
                tot_ent += (sub_ent*(len(v)/docs_len))
    return tot_ent


def get_entropy_20news(cluster_lists, cluster_num, dataset_no):
    tot_ent = 0
    sub_ent = 0
    docs_len = 0

    # retrieve original ccluster dictionary
    paths = ['./source/20news_150.classes', './source/20news_828.classes', './source/20news_full.classes']
    for k, v in cluster_lists.items():
        docs_len += len(v)

    tlabels = get_20newsgroup_tlabels(paths[dataset_no], 20)

    # list for
    max_list = []
    for k, v in cluster_lists.items():
        temp = {'C{}'.format(i): [] for i in range(cluster_num)}
        for it in v:
            if it in tlabels['C0']:
                temp['C0'].append(it)
            elif it in tlabels['C1']:
                temp['C1'].append(it)
            elif it in tlabels['C2']:
                temp['C2'].append(it)
            elif it in tlabels['C3']:
                temp['C3'].append(it)
            elif it in tlabels['C4']:
                temp['C4'].append(it)
            elif it in tlabels['C5']:
                temp['C5'].append(it)
            elif it in tlabels['C6']:
                temp['C6'].append(it)
            elif it in tlabels['C7']:
                temp['C7'].append(it)
            elif it in tlabels['C8']:
                temp['C8'].append(it)
            elif it in tlabels['C9']:
                temp['C9'].append(it)
            elif it in tlabels['C10']:
                temp['C10'].append(it)
            elif it in tlabels['C11']:
                temp['C11'].append(it)
            elif it in tlabels['C12']:
                temp['C12'].append(it)
            elif it in tlabels['C13']:
                temp['C13'].append(it)
            elif it in tlabels['C14']:
                temp['C14'].append(it)
            elif it in tlabels['C15']:
                temp['C15'].append(it)
            elif it in tlabels['C16']:
                temp['C16'].append(it)
            elif it in tlabels['C17']:
                temp['C17'].append(it)
            elif it in tlabels['C18']:
                temp['C18'].append(it)
            elif it in tlabels['C19']:
                temp['C19'].append(it)
        for k1, v1 in temp.items():
            if len(v1) == 0:
                pass
            else:
                sub_ent += (len(v1)/len(v))*math.log(len(v1)/len(v), 2)
                tot_ent += (sub_ent*(len(v)/docs_len))
    return tot_ent


##########################################################
# Function for saving factorization results to CSV files #
##########################################################
def save2csv(Y, A, X, clu_num, features, filenames):
    YF = pd.DataFrame(Y.toarray())
    AF = pd.DataFrame(A)
    XF = pd.DataFrame(X)

    YF.to_csv('./results/{}.csv'.format(filenames[0]), sep=',', header=features)
    AF.to_csv('./results/{}.csv'.format(filenames[1]), sep=',', header=['C{}'.format(c) for c in range(clu_num)])
    XF.to_csv('./results/{}.csv'.format(filenames[2]), sep=',', header=features)
